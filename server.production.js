var express = require('express');
var app = express();

app.use('/', express.static('static'));

app.listen(80, '0.0.0.0', function () {
  console.log('Example app listening on port 80!');
});
