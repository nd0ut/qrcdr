const INT_1 = 1;
const INT_2 = 2;
const INT_3 = 3;
const INT_4 = 4;

class SvgRenderer {
  matrix = [];
  shape = 0;
  svgSize = 400;
  width = 0;
  dpi = 2;
  offset = 1;
  roundOuter = 5;
  roundInner = 5;
  outerAngle = 0;
  innerAngle = 0;

  singleCell = 1;
  pairedCells = 2;
  groupedCells = 3;

  colorBack = 0;
  colorPath = 0;
  colorNone = 0;
  colorSingle = 0;
  colorGroup = 0;
  colorPair = 0;

  setOptions(options) {
    options.dpi !== undefined && (this.dpi = options.dpi);
    options.shape !== undefined && (this.shape = options.shape);
    options.svgSize !== undefined && (this.svgSize = options.svgSize);
    options.roundOuter !== undefined && (this.roundOuter = options.roundOuter);
    options.roundInner !== undefined && (this.roundInner = options.roundInner);
    options.offset !== undefined && (this.offset = options.offset);
    options.outerAngle !== undefined && (this.outerAngle = options.outerAngle);
    options.innerAngle !== undefined && (this.innerAngle = options.innerAngle);

    options.colorBack !== undefined && (this.colorBack = options.colorBack);
    options.colorPath !== undefined && (this.colorPath = options.colorPath);
    options.colorNone !== undefined && (this.colorNone = options.colorNone);
    options.colorSingle !== undefined && (this.colorSingle = options.colorSingle);
    options.colorGroup !== undefined && (this.colorGroup = options.colorGroup);
    options.colorPair !== undefined && (this.colorPair = options.colorPair);
  }

  renderToString(qrcode, options, someArg) {
    this.setOptions(options);

    this.matrix = qrcode.matrix;
    let width = (this.matrix.length + 2 * this.offset);
    this.width = width * this.dpi;
    let svg = '<svg width="' + this.svgSize + 'px" height="' + this.svgSize + 'px" viewBox="0 0 ' + this.width + ' ' + this.width + '" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">';
    svg += '<g id="qr-qrcode"><defs></defs>';
    svg += this.process(someArg);
    svg += '</g>';
    svg += '</svg>';
    return svg
  }

  process(someArg) {
      let w = (this.width);
      let size = 'width="' + w + 'px" height="' + w + 'px"';
      let markup = '<rect fill="' + (this.colorBack != 0 ? this.colorBack : '#fff') + '" ' + size + ' x="0" y="0" class="qr-bg"/>';
      let splines = this.makeSplines()
      let L = '';

      markup += '<g id="qr-image_bg"></g>';
      for (let I = 0; I < splines.length; I++) {
          L += this.shapePath(splines[I], I)
      }
      markup += L;
      markup += '<g id="qr-images"></g>';
      if (!someArg) {
          L = '';
          for (let I in this.matrix) {
              if (I != parseInt(I)) {
                  break
              }
              for (let H in this.matrix[I]) {
                  if (I != parseInt(I)) {
                      break
                  }
                  if ((H > 8 && H < this.matrix.length - 8 && I != 6 || I > 8 && I < this.matrix.length - 8 && H != 6 || (I > this.matrix.length - 9 && H > this.matrix.length - 9)) && (this.matrix.length <= 21 || !(I > this.matrix.length - 10 && H > this.matrix.length - 10 && I < this.matrix.length - 4 && H < this.matrix.length - 4))) {
                      L += '<rect x="' + ((parseInt(I) + this.offset) * this.dpi) + '" y="' + ((parseInt(H) + this.offset) * this.dpi) + '" _x="' + I + '" _y="' + H + '" width="' + this.dpi + '" height="' + this.dpi + '" class="msk" fill="#fff" style="display: none;" stroke-width="0"/>'
                  } else {
                      L += '<rect x="' + ((parseInt(I) + this.offset) * this.dpi) + '" y="' + ((parseInt(H) + this.offset) * this.dpi) + '" _x="' + I + '" _y="' + H + '" width="' + this.dpi + '" height="' + this.dpi + '" class="mskc" fill="#f00" style="display: none; opacity: 0.4;" stroke-width="0"/>'
                  }
              }
          }
          markup += L
      }
      return markup
  }

  makeSplines() {
      let len = this.matrix.length;
      let K = '';
      let H = [];
      let J = {
          cache: ''
      };
      for (let L = 0; L < len; L++) {
          for (let w = 0; w < len; w++) {
              if (this.matrix[L][w] && !this.isCached('%' + L + '-' + w + '%', J)) {
                  let I = [];
                  this.setShapePoints(L, w, I, J);
                  H.push(I)
              }
          }
      }
      return H
  }
  isCached(H, w) {
      if (~w.cache.indexOf(H)) {
          return true
      } else {
          w.cache = H + w.cache;
          return false
      }
  }
  setShapePoints(J, w, I, H) {
      if (I[w] === undefined) {
          I[w] = []
      }
      I[w][J] = 1;
      if (this.shape == 0 || this.shape == 1) {
          if (this.matrix[J - 1] !== undefined && this.matrix[J - 1][w]) {
              if (!this.isCached('%' + (J - 1) + '-' + w + '%', H)) {
                  this.setShapePoints(J - 1, w, I, H)
              }
          }
          if (this.matrix[J][w + 1] !== undefined && this.matrix[J][w + 1]) {
              if (!this.isCached('%' + (J) + '-' + (w + 1) + '%', H)) {
                  this.setShapePoints(J, w + 1, I, H)
              }
          }
          if (this.matrix[J + 1] !== undefined && this.matrix[J + 1][w]) {
              if (!this.isCached('%' + (J + 1) + '-' + w + '%', H)) {
                  this.setShapePoints(J + 1, w, I, H)
              }
          }
          if (this.matrix[J][w - 1] !== undefined && this.matrix[J][w - 1]) {
              if (!this.isCached('%' + (J) + '-' + (w - 1) + '%', H)) {
                  this.setShapePoints(J, w - 1, I, H)
              }
          }
      }
      if (this.shape == 1 || this.shape == 2) {
          if (this.matrix[J - 1] !== undefined && this.matrix[J - 1][w - 1] !== undefined && this.matrix[J - 1][w - 1]) {
              if (!this.isCached('%' + (J - 1) + '-' + (w - 1) + '%', H)) {
                  this.setShapePoints(J - 1, w - 1, I, H)
              }
          }
          if (this.matrix[J - 1] !== undefined && this.matrix[J - 1][w + 1] !== undefined && this.matrix[J - 1][w + 1]) {
              if (!this.isCached('%' + (J - 1) + '-' + (w + 1) + '%', H)) {
                  this.setShapePoints(J - 1, w + 1, I, H)
              }
          }
          if (this.matrix[J + 1] !== undefined && this.matrix[J + 1][w + 1] !== undefined && this.matrix[J + 1][w + 1]) {
              if (!this.isCached('%' + (J + 1) + '-' + (w + 1) + '%', H)) {
                  this.setShapePoints(J + 1, w + 1, I, H)
              }
          }
          if (this.matrix[J + 1] !== undefined && this.matrix[J + 1][w - 1] !== undefined && this.matrix[J + 1][w - 1]) {
              if (!this.isCached('%' + (J + 1) + '-' + (w - 1) + '%', H)) {
                  this.setShapePoints(J + 1, w - 1, I, H)
              }
          }
      }
      return true
  }
  closePath(H, w, I) {
      if (!H.length) {
          H.push('L ' + (w) + ' ' + (I));
          H.push('M ' + (w) + ' ' + (I))
      } else {
          H.push('L ' + (w) + ' ' + (I))
      }
  }
  smoothPath(L, w, M, I, K, H, J) {
      L.push(' C ' + (w) + ' ' + M + ' ' + (w + I) + ' ' + (M + K) + ' ' + (w + H) + ' ' + (M + J) + ' ')
  }
  shapePath(H, V) {
      let J = this.groupPaths(H);
      let P = '';
      let aa = '';
      let cellClass = '';
      let roundOuterDivA = this.dpi / this.roundOuter;
      let roundInnerDivA = this.dpi / this.roundInner;
      let roundOuterSmth = (this.outerAngle);
      let roundInnerSmth = (this.innerAngle);
      let roundOuterUnder12 = (this.roundOuter < 12);
      let roundInnerUnder12 = (this.roundInner < 12);
      for (let Y in J.path) {
          if (Y != parseInt(Y)) {
              break
          }
          let N = J.path[Y];
          let Q = [];
          let X = 0;
          N.push(N[0]);
          for (let S = 0; S < N.length; S++) {
              let O = N[S].points;
              let I = N[S].type;
              let M = (O[0][1] + this.offset) * this.dpi;
              let L = (O[0][0] + this.offset) * this.dpi;

              if (I != X) {
                  if (roundOuterUnder12) {
                      if (X == INT_2 && I == INT_3) {
                          M = M - roundOuterDivA + roundOuterSmth;
                          this.closePath(Q, M, L);
                          this.smoothPath(Q, M, L, roundOuterDivA, 0, roundOuterDivA, roundOuterDivA)
                      } else {
                          if (X == INT_3 && I == INT_4) {
                              L = L - roundOuterDivA + roundOuterSmth;
                              this.closePath(Q, M, L);
                              this.smoothPath(Q, M, L, 0, roundOuterDivA, -1 * roundOuterDivA, roundOuterDivA)
                          } else {
                              if (X == INT_4 && I == INT_1) {
                                  M = M + roundOuterDivA - roundOuterSmth;
                                  this.closePath(Q, M, L);
                                  this.smoothPath(Q, M, L, -1 * roundOuterDivA, 0, -1 * roundOuterDivA, -1 * roundOuterDivA)
                              } else {
                                  if (X == INT_1 && I == INT_2) {
                                      L = L + roundOuterDivA - roundOuterSmth;
                                      this.closePath(Q, M, L);
                                      this.smoothPath(Q, M, L, 0, -1 * roundOuterDivA, roundOuterDivA, -1 * roundOuterDivA)
                                  }
                              }
                          }
                      }
                  } else {
                      if (X == INT_2 && I == INT_3 || X == INT_3 && I == INT_4 || X == INT_4 && I == INT_1 || X == INT_1 && I == INT_2) {
                          this.closePath(Q, M, L)
                      }
                  }
                  if (roundInnerUnder12) {
                      if (X == INT_2 && I == INT_1) {
                          M = M - roundInnerDivA + roundInnerSmth;
                          this.closePath(Q, M, L);
                          this.smoothPath(Q, M, L, roundInnerDivA, 0, roundInnerDivA, -1 * roundInnerDivA)
                      } else {
                          if (X == INT_1 && I == INT_4) {
                              L = L + roundInnerDivA - roundInnerSmth;
                              this.closePath(Q, M, L);
                              this.smoothPath(Q, M, L, 0, -1 * roundInnerDivA, -1 * roundInnerDivA, -1 * roundInnerDivA)
                          } else {
                              if (X == INT_4 && I == INT_3) {
                                  M = M + roundInnerDivA - roundInnerSmth;
                                  this.closePath(Q, M, L);
                                  this.smoothPath(Q, M, L, -1 * roundInnerDivA, 0, -1 * roundInnerDivA, roundInnerDivA)
                              } else {
                                  if (X == INT_3 && I == INT_2) {
                                      L = L - roundInnerDivA + roundInnerSmth;
                                      this.closePath(Q, M, L);
                                      this.smoothPath(Q, M, L, 0, roundInnerDivA, roundInnerDivA, roundInnerDivA)
                                  }
                              }
                          }
                      }
                  } else {
                      if (X == INT_2 && I == INT_1 || X == INT_1 && I == INT_4 || X == INT_4 && I == INT_3 || X == INT_3 && I == INT_2) {
                          this.closePath(Q, M, L)
                      }
                  }
              }
              X = I
          }
          Q.push(Q.shift());
          P += Q.join('')
      }
      cellClass = (J.pclass == this.singleCell ? 'class="q-one"' + (this.colorSingle != 0 ? ' fill="' + this.colorSingle + '"' : '') : '');
      cellClass += (J.pclass == this.pairedCells ? 'class="q-sq"' + (this.colorPair != 0 ? ' fill="' + this.colorPair + '"' : '') : '');
      cellClass += (J.pclass == this.groupedCells ? 'class="q-sqf"' + (this.colorGroup != 0 ? ' fill="' + this.colorGroup + '"' : '') : '');
      aa += '<path id="p' + V + '" ' + (cellClass != '' ? cellClass : (this.colorPath != 0 ? 'fill="' + this.colorPath + '"' : '')) + ' d="' + P + ' z" />';
      return aa
  }
  groupPaths(O) {
      let N = O.length;
      let T = 0;
      let W = [];
      let S = {
          cache: ''
      };
      let R = [];
      let Q = [];
      let M = -1;
      let P = 0;
      let I = 0;
      let K = 0;
      let H = '';
      let L = '';
      for (let V in O) {
          if (V != parseInt(V)) {
              break
          }
          T = O[V].length;
          for (let J in O[V]) {
              if (J != parseInt(J)) {
                  break
              }
              this.getSquares(parseInt(J), parseInt(V), O, W, S)
          }
      }
      while (W.length > 0) {
          if (M == -1) {
              Q[0] = W.shift();
              M++
          }
          let U = this.getShapeCorner(Q[M].type, Q[M].points[1][0], Q[M].points[1][1], W);
          if (U != -1) {
              M++;
              I++;
              Q[M] = W[U];
              W.splice(U, 1);
              if ((Q[M].points[1][0] == Q[0].points[0][0]) && (Q[M].points[1][1] == Q[0].points[0][1])) {
                  R.push(Q);
                  Q = [];
                  M = -1
              }
          } else {
              break
          }
          K++;
          if (K > 40000) {
              R.push(Q);
              Q = [];
              break
          }
      }
      if (I == 3) {
          P = this.singleCell
      }
      if ((N == 1 || N == 7 || T == 1 || T == 7) && N < 8 && T < 8 || (N == 1 || N == 7 || T == this.matrix.length || T == this.matrix.length - 6) && N < 8 && T > this.matrix.length - 7 || (N == this.matrix.length || N == this.matrix.length - 6 || T == 1 || T == 7) && N > this.matrix.length - 7 && T < 8) {
          P = this.pairedCells
      }
      if (N > 2 && N < 6 && T > 2 && T < 6 || N > 2 && N < 6 && T < this.matrix.length - 1 && T > this.matrix.length - 5 || N < this.matrix.length - 1 && N > this.matrix.length - 5 && T > 2 && T < 6) {
          P = this.groupedCells
      }
      return {
          path: R,
          pclass: P
      }
  }
  getShapeCorner(L, M, w, I) {
      let K = [];
      for (let J in I) {
          if (J != parseInt(J)) {
              break
          }
          let H = I[J];
          if (M == H.points[0][0] && w == H.points[0][1]) {
              K[H.type] = J
          }
      }
      if (this.shape == 1 || this.shape == 2) {
          if (L == INT_1) {
              if (K[INT_4] !== undefined) {
                  return K[INT_4]
              }
              if (K[INT_1] !== undefined) {
                  return K[INT_1]
              }
              if (K[INT_2] !== undefined) {
                  return K[INT_2]
              }
          } else {
              if (L == INT_2) {
                  if (K[INT_1] !== undefined) {
                      return K[INT_1]
                  }
                  if (K[INT_2] !== undefined) {
                      return K[INT_2]
                  }
                  if (K[INT_3] !== undefined) {
                      return K[INT_3]
                  }
              } else {
                  if (L == INT_3) {
                      if (K[INT_2] !== undefined) {
                          return K[INT_2]
                      }
                      if (K[INT_3] !== undefined) {
                          return K[INT_3]
                      }
                      if (K[INT_4] !== undefined) {
                          return K[INT_4]
                      }
                  } else {
                      if (L == INT_4) {
                          if (K[INT_3] !== undefined) {
                              return K[INT_3]
                          }
                          if (K[INT_4] !== undefined) {
                              return K[INT_4]
                          }
                          if (K[INT_1] !== undefined) {
                              return K[INT_1]
                          }
                      }
                  }
              }
          }
      } else {
          if (L == INT_1) {
              if (K[INT_2] !== undefined) {
                  return K[INT_2]
              }
              if (K[INT_4] !== undefined) {
                  return K[INT_4]
              }
              if (K[INT_1] !== undefined) {
                  return K[INT_1]
              }
          } else {
              if (L == INT_2) {
                  if (K[INT_3] !== undefined) {
                      return K[INT_3]
                  }
                  if (K[INT_1] !== undefined) {
                      return K[INT_1]
                  }
                  if (K[INT_2] !== undefined) {
                      return K[INT_2]
                  }
              } else {
                  if (L == INT_3) {
                      if (K[INT_4] !== undefined) {
                          return K[INT_4]
                      }
                      if (K[INT_2] !== undefined) {
                          return K[INT_2]
                      }
                      if (K[INT_3] !== undefined) {
                          return K[INT_3]
                      }
                  } else {
                      if (L == INT_4) {
                          if (K[INT_1] !== undefined) {
                              return K[INT_1]
                          }
                          if (K[INT_3] !== undefined) {
                              return K[INT_3]
                          }
                          if (K[INT_4] !== undefined) {
                              return K[INT_4]
                          }
                      }
                  }
              }
          }
      }
      return -1
  }
  getSquares(w, L, J, I, K) {
      if (J[L] === undefined || J[L][w - 1] === undefined) {
          let H = {
              points: [],
              type: ''
          };
          H.points[0] = [L + 1, w];
          H.points[1] = [L, w];
          H.type = INT_1;
          if (!this.isHashCached(H, K)) {
              I.push(H)
          }
      }
      if (J[L - 1] === undefined || J[L - 1][w] === undefined) {
          let H = {
              points: [],
              type: ''
          };
          H.points[0] = [L, w];
          H.points[1] = [L, w + 1];
          H.type = INT_2;
          if (!this.isHashCached(H, K)) {
              I.push(H)
          }
      }
      if (J[L] === undefined || J[L][w + 1] === undefined) {
          let H = {
              points: [],
              type: ''
          };
          H.points[0] = [L, w + 1];
          H.points[1] = [L + 1, w + 1];
          H.type = INT_3;
          if (!this.isHashCached(H, K)) {
              I.push(H)
          }
      }
      if (J[L + 1] === undefined || J[L + 1][w] === undefined) {
          let H = {
              points: [],
              type: ''
          };
          H.points[0] = [L + 1, w + 1];
          H.points[1] = [L + 1, w];
          H.type = INT_4;
          if (!this.isHashCached(H, K)) {
              I.push(H)
          }
      }
  }
  isHashCached(w, H) {
      let hash = w.type + '=' + w.points[0][0] + '-' + w.points[0][1] + '-' + w.points[1][0] + '-' + w.points[1][1];
      if (~H.cache.indexOf(hash)) {
          return true
      } else {
          H.cache = H.cache + '%' + hash + '%';
          return false
      }
  }
}

export default new SvgRenderer();
