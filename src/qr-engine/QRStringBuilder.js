export default class QRStringBuilder {
  static build(type, data) {
    return this[type + 'Handler'](data);
  }

  static textHandler(data) {
    return data.plain_text;
  }

  static linkHandler(data) {
    return 'http://' + data.url;
  }

  static contactHandler(data) {
    const str = (
      'BEGIN:VCARD\n' +
      'VERSION:3.0\n' +
      `N:${data.last_name || ''};${data.name || ''};${data.patronymic_name || ''}\n` +
      `ORG:${data.organization || ''}\n` +
      `TITLE:${data.position || ''}\n` +
      `TEL;TYPE=voice:${data.mobile_phone || ''}\n` +
      `TEL;TYPE=work:${data.work_phone || ''}\n` +
      `EMAIL;TYPE=INTERNET:${data.email || ''}\n` +
      `URL:${data.url || ''}\n` +
      `ADR:;;${data.street || ''} ${data.building || ''};${data.city || ''};;;${data.country || ''}\n` +
      'END:VCARD'
    );

    return str;
  }

  static wifiHandler(data) {
    return `WIFI:S:${data.ssid};T:${data.security};P:${data.password};H:${data.hidden ? 'true' : 'false'};`;
  }

  static smsHandler(data) {
    return `SMSTO:${data.phone}:${data.text}`;
  }

  static emailHandler(data) {
    return `MATMSG:TO:${data.email};SUB:${data.subject};BODY:${data.body};;`;
  }

  static phoneHandler(data) {
    return `tel:${data.phone}`;
  }

  static geoHandler(data) {
    return `geo:${data.lat},${data.lon},100`;
  }

  static googleplayHandler(data) {
    return `market://details?id=${data.package_name}`
  }

  static youtubeHandler(data) {
    return `youtube://${data.video_id}`;
  }

  static calendarHandler(data) {
    const startDate = new Date(data.startDate).toJSON().replace(/[:-]/g, '');
    const endDate = new Date(data.endDate).toJSON().replace(/[:-]/g, '');

    return (
      'BEGIN:VEVENT\n' +
      `SUMMARY:${data.summary}\n` +
      `DTSTART:${startDate}\n` +
      `DTEND:${endDate}\n` +
      'END:VEVENT'
    );
  }
}
