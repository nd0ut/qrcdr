import * as d3 from 'd3';
import SvgRenderer from './SvgRenderer';

export default class QRCodeRenderer {
  static render(qrcode, options) {
    const svg = SvgRenderer.renderToString(qrcode, options);
    return svg;
  }
}
