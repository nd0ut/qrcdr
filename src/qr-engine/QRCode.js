import Immutable from 'immutable';

const defaultOptions = {
  cells: null,
  matrix: null,
  value: null,
  width: null,
  level: null,
  version: null
};

export const QRCodeRecord = Immutable.Record(defaultOptions, 'QRCode');

export default class QRCode extends QRCodeRecord  {
  constructor(opts) {
    const cells = opts.matrix.map((val, idx) => {
      const col = Math.floor(idx / opts.width);
      const row = idx - opts.width * col;
      return {dark: !!val, col, row};
    });

    const matrix = new Array(opts.width);
    for (let i = 0; i < matrix.length; i++) {
      matrix[i] = new Array(opts.width);
    }

    cells.forEach(cell => {
      matrix[cell.row][cell.col] = cell.dark;
    });

    opts = {
      ...opts,
      cells,
      matrix
    };

    super(opts);
  }

  _darkCells = null;
  getDarkCells() {
    if(this._darkCells) {
      return this._darkCells;
    }

    const cells = this.cells
      .filter(cell => cell.dark)
      .map(cell => [cell.row, cell.col]);

    this._darkCells = cells;
    return cells;
  }
}
