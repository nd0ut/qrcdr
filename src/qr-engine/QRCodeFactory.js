import QRCode from './QRCode';
import {QRCodeDraw} from 'qrcode';

export default class QRCodeFactory {
  make(options) {
    return new Promise((res, rej) => {
      const input = [{data: options.value, mode: 'byte'}];
      const opts = {errorCorrectionLevel: options.level, version: options.version};

      new QRCodeDraw().drawBitArray(input, opts, (error, bits, width) => {
        if(error) {
          return rej(error);
        }

        const qrcode = new QRCode({
          matrix: bits,
          level: options.level,
          version: options.version,
          value: options.value,
          width
        });
        res(qrcode);
      });
    })
  }
}
