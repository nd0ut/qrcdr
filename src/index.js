import {} from 'styles/root.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Routing from './components/Routing';
import QRCodeFactory from 'qr-engine/QRCodeFactory';
import store from 'redux/store';

ReactDOM.render(<Routing store={store} />, document.getElementById('root'));
