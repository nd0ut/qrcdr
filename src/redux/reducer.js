import { handleActions } from 'redux-actions';
import Immutable from 'immutable';

import QRCodeFactory from 'qr-engine/QRCodeFactory';


const initialState = Immutable.fromJS({
  qrContent: null,
  qrCode: null,
  qrOptions: new Immutable.Map({
    dpi: 2,
    shape: 0,
    svgSize: 500,
    roundOuter: 5,
    roundInner: 5,
    offset: 1,
    outerAngle: 0,
    innerAngle: 0,
    colorBack: '#ffffff',
    colorPath: '#000000',
    colorNone: '#000000',
    colorSingle: '#000000',
    colorGroup: '#000000',
    colorPair: '#000000'
  }),
  qrImages: null
});

const reducer = handleActions({
  'SET_QR_CONTENT': setQrContent,
  'SET_QR_OPTIONS': setQrOptions,

  'ADD_QR_IMAGES': addQrImages,
  'REMOVE_QR_IMAGE': removeQrImage,
  'UPDATE_QR_IMAGE': updateQrImage,
}, initialState);

function setQrContent(state, action) {
  const {type, data, qrCode} = action.payload;
  const qrContent = Immutable.fromJS({type, data});

  return state
    .set('qrCode', qrCode)
    .set('qrContent', qrContent)
}

function setQrOptions(state, {payload: qrOptions}) {
  const nextOptions = state.get('qrOptions').mergeDeepWith((prev, next, key) => next !== undefined ? next : prev,qrOptions);
  return state.set('qrOptions', nextOptions);
}

function addQrImages(state, {payload: qrImages}) {
  const currentImages = state.get('qrImages') || new Immutable.List();
  return state.set('qrImages', currentImages.concat(Immutable.fromJS(qrImages)));
}

function updateQrImage(state, {payload: {id, data}}) {
  const nextImages = state.get('qrImages').map(image => {
    if(image.get('id') !== id) {
      return image;
    }
    return image.merge(data);
  });

  return state.set('qrImages', nextImages);
}

function removeQrImage(state, {payload: id}) {
  return state.set('qrImages', state.get('qrImages').filter(img => img.get('id') !== id));
}

export default reducer;
