import {
  createStore,
  applyMiddleware,
  combineReducers,
  compose
} from 'redux';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import {persistStore, autoRehydrate} from 'redux-persist';
import immutableTransform from 'redux-persist-transform-immutable';
import createFilter from './libs/redux-persist-transform-filter-immutable';

import appReducer from './reducer';

import {QRCodeRecord} from '../qr-engine/QRCode';

const logger = createLogger();

const middleware = [
  thunk,
  promise,
  logger
];

const reducer = combineReducers({app: appReducer});
const store = createStore(reducer, {}, compose(
  applyMiddleware(...middleware),
  autoRehydrate(),
  window.devToolsExtension ? window.devToolsExtension() : f => f
));

const saveSubsetFilter = createFilter(
  'app',
  ['qeCode', 'qrContent', 'qrOptions']
);

persistStore(store, {transforms: [immutableTransform({records: [QRCodeRecord]})]});

window.store = store;

export default store;
