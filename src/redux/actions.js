import { createAction } from 'redux-actions';
import QRStringBuilder from 'qr-engine/QRStringBuilder';
import QRCodeFactory from 'qr-engine/QRCodeFactory';

const setQrContentAction = createAction('SET_QR_CONTENT');

export const setQrContent = ({type, data, level, version}) => dispatch => {
  const qrString = QRStringBuilder.build(type, data);
  new QRCodeFactory()
    .make({level, version, value: qrString})
    .then(qrCode => dispatch(setQrContentAction({type, data, qrCode})));
}

export const setQrOptions = createAction('SET_QR_OPTIONS');
export const addQrImages = createAction('ADD_QR_IMAGES');
export const removeQrImage = createAction('REMOVE_QR_IMAGE');
export const updateQrImage = createAction('UPDATE_QR_IMAGE', (id, data) => ({id, data}));
