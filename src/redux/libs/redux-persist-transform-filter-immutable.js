import { Iterable, Map } from 'immutable';
import { createTransform } from 'redux-persist';
import get from 'lodash.get';
import set from 'lodash.set';
import {isUndefined} from 'lodash';


export default (reducerName, inboundPaths, outboundPaths) => {
    return createTransform(
        (inboundState, key) => inboundPaths ? persistFilter(inboundState, inboundPaths) : inboundState,
        (outboundState, key) => outboundPaths ? persistFilter(outboundState, outboundPaths) : outboundState,
        {whitelist: [reducerName]}
    );
};

export function persistFilter (state, paths): returnType {
    let iterable  = Iterable.isIterable(state);
    let subset = iterable ? Map({}) : {};

    (_.isString(paths) ? [paths] : paths).forEach((path) => {
        let value = iterable ? state.get(path) : get(state, path);
        if(!isUndefined(value)) {
            iterable ? (subset = (subset).set(path, value)) : set(subset, path, value);
        }
    });

    return subset;
}
