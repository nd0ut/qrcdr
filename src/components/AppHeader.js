import {} from 'styles/AppHeader.css';

import React, { Component } from 'react';
import { Grid, Segment, Image } from 'semantic-ui-react'

export default class AppHeader extends Component {
  render() {
    return (
      <Grid.Row centered>
        <Grid.Column>
          <a className='app-header' href='/'>
            {/*<i className='fa fa-superpowers app-header__logo' aria-hidden='true'></i>*/}
            <div className='app-header__logo'>
              <div className='app-header__title'>Каррр</div>
              <div className='app-header__crow'><Image width='120' src='/images/crow.png'/></div>
              <div className='app-header__title'>Кодер</div>
            </div>
          </a>
        </Grid.Column>
      </Grid.Row>
    );
  }
}

