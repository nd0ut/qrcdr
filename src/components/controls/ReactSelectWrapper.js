import Select from 'react-select';
import 'styles/controls/ReactSelectWrapper.css';
import {autobind} from 'core-decorators';
import React, { Component } from 'react';
import param from 'jquery-param';
import { throttle } from 'lodash';
import { Image, Item, Label, Icon} from 'semantic-ui-react'

export default class ReactSelectWrapper extends Component {
  state = {
    value: null,
    error: null
  }

  searchGooglePlay(query) {
    const params = {
      access_token: '36073bc0716fe9a85484adcdb29cd0c1f9ada8e9',
      q: query,
      limit: 5,
      lang: 'ru',
      app_country: 'RU',
      available_in: 'RU'
    };

    return fetch('https://data.42matters.com/api/v2.0/android/apps/search.json?' + param(params))
      .then(res => res.json())
      .then(res => res.results);
  }

  searchYoutube(query) {
    const params = {
      part: 'snippet',
      q: query
    };

    return fetch('https://www.googleapis.com/youtube/v3/search?' + param(params))
      .then(res => res.json())
      .then(res => {
        if(res.error) {
          throw res.error.message;
          return res.error.message;
        }
        return res;
      })
      .then(res => res.items);
  }

  @autobind
  renderOption(option) {
    let icon, title, description;
    if(this.props.googleplay) {
      icon = option.app.icon_72;
      title = option.app.title;
      description = option.app.short_desc;
    }
    else if(this.props.youtube) {
      icon = option.video.snippet.thumbnails.default.url;
      title = option.video.snippet.title;
      description = option.video.snippet.description;
    }

    return (
      <Item.Group>
      <Item>
        <Item.Image size='tiny' src={icon} />
        <Item.Content>
          <Item.Header as='a'>{title}</Item.Header>
          <Item.Description>{description}</Item.Description>
        </Item.Content>
      </Item>
      </Item.Group>
    );
  }

  @autobind
  renderValue(option) {
    let icon, title;
    if(this.props.googleplay) {
      icon = option.app.icon_72;
      title = option.app.title;
    }
    else if(this.props.youtube) {
      icon = option.video.snippet.thumbnails.default.url;
      title = option.video.snippet.title;
    }

    return (
      <Label>
        <Image avatar spaced='right' src={icon} />
        {title}
      </Label>
    );
  }

  @autobind
  _getOptions(input, cb) {
    if(this.props.googleplay) {
      this.searchGooglePlay(input)
        .then(results => results.map(app => ({app, value: app.package_name, label: app.title})))
        .then(options => cb(null, {options}))
        .catch(error => {
          this.setState({error});
          cb(null, {complete: true});
        });
    }
    else if(this.props.youtube) {
      this.searchYoutube(input)
        .then(results => results.map(video => ({video, value: video.id.videoId, label: video.snippet.title})))
        .then(options => cb(null, {options}))
        .catch(error => {
          this.setState({error});
          cb(null, {complete: true});
        });
    }
  }
  getOptions = throttle(this._getOptions, 300);

  @autobind
  onChange(value) {
    this.setState({value});
  }

  render() {
    return (
      <div>
        {this.state.error ? <Label color='red'><Icon name='exclamation triangle' />{this.state.error.toString()}</Label> : null}
        <Select.Async
            name={this.props.name}
            value={this.state.value}
            loadOptions={this.getOptions}
            onChange={this.onChange}
            optionRenderer={this.renderOption}
            valueRenderer={this.renderValue}
            searchPromptText='Поиск'
            noResultsText='Не найдено'
            clearValueText='Очистить'
            clearAllText='Очистить'
            addLabelText='Добавить'
            placeholder='Введите название'
            loadingPlaceholder='Загрузка...'
        />
      </div>
    );
  }
}
