import {} from 'styles/customization/ColorsTab.css';

import React, { Component } from 'react';
import { Step, Button } from 'semantic-ui-react'
import QRCodeView from 'components/QRCodeView';
import {connect} from 'react-redux';
import Tabs, { TabPane } from 'rc-tabs';
import TabContent from 'rc-tabs/lib/TabContent';
import InkTabBar from 'rc-tabs/lib/InkTabBar';
import Slider from 'rc-slider';
import {autobind, decorate} from 'core-decorators';
import { memoize, debounce, random, sample} from 'lodash';
import { Form, Checkbox, Menu, Image } from 'semantic-ui-react'
import { CirclePicker, SketchPicker } from 'react-color';

const COLORS = ['#ffffff', '#000000', '#f44336', '#e91e63', '#9c27b0', '#673ab7', '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4', '#009688', '#4caf50', '#8bc34a', '#cddc39', '#ffeb3b', '#ffc107', '#ff9800', '#ff5722', '#795548', '#607d8b'];

export default class ColorsTab extends Component {
  state = {
    selectedColorType: 'Back',
    pickerVisible: false,
    pickerCoords: [0,0]
  }

  componentDidMount() {
    document.addEventListener('click', e => {
      if(!e.target.closest('.sketch-picker') && !e.target.closest('.colors-tab__show-picker-btn')) {
        this.setState({
          pickerVisible: false
        });
      }
    });
  }

  @autobind
  makeRandom() {
    const randomOptions = {
      colorBack: sample(COLORS),
      colorPath: sample(COLORS),
      colorNone: sample(COLORS),
      colorSingle: sample(COLORS),
      colorGroup: sample(COLORS),
      colorPair: sample(COLORS)
    };

    this.props.appActions.setQrOptions(randomOptions);
  }

  @autobind
  reset() {
    const defaultOptions = {
      colorBack: '#ffffff',
      colorPath: '#000000',
      colorNone: '#000000',
      colorSingle: '#000000',
      colorGroup: '#000000',
      colorPair: '#000000'
    };

    this.props.appActions.setQrOptions(defaultOptions);
  }

  @autobind
  showPicker(e) {
    const coords = [e.target.offsetLeft - 220 - 10, e.target.offsetTop - 306 + e.target.offsetHeight];

    this.setState({
      pickerVisible: !this.state.pickerVisible,
      pickerCoords: coords
    });
  }

  @autobind
  onColorChange(color) {
    const colorType = this.state.selectedColorType;
    const hex = color.hex;

    this.props.appActions.setQrOptions({
      ['color' + colorType]: hex
    });
  }

  @decorate(memoize)
  onColorTypeChangeFn(colorType) {
    return () => {
      this.setState({selectedColorType: colorType})
    }
  }

  colorTypeSelector() {
    const qrOptions = this.props.qrOptions.toJS();
    const colorType = this.state.selectedColorType;

    return (
      <div className='colors-tab__color-type-select'>
        <Menu fluid vertical secondary>
          <Menu.Item name='Background' active={colorType === 'Back'} onClick={this.onColorTypeChangeFn('Back')}>
            <div className='colors-tab__color-icon' style={{backgroundColor: qrOptions.colorBack}}/>
            Фон
          </Menu.Item>

          <Menu.Item name='Path' active={colorType === 'Path'} onClick={this.onColorTypeChangeFn('Path')}>
            <div className='colors-tab__color-icon' style={{backgroundColor: qrOptions.colorPath}}/>
            Соседние ячейки
          </Menu.Item>

{/*          <Menu.Item name='None' active={colorType === 'None'} onClick={this.onColorTypeChangeFn('None')}>
            None
          </Menu.Item>
*/}
          <Menu.Item name='Single' active={colorType === 'Single'} onClick={this.onColorTypeChangeFn('Single')}>
            <div className='colors-tab__color-icon' style={{backgroundColor: qrOptions.colorSingle}}/>
            Одиночные ячейки
          </Menu.Item>

          <Menu.Item name='Group' active={colorType === 'Group'} onClick={this.onColorTypeChangeFn('Group')}>
            <div className='colors-tab__color-icon' style={{backgroundColor: qrOptions.colorGroup}}/>
            Поисковый узор внутри
          </Menu.Item>

          <Menu.Item name='Pair' active={colorType === 'Pair'} onClick={this.onColorTypeChangeFn('Pair')}>
            <div className='colors-tab__color-icon' style={{backgroundColor: qrOptions.colorPair}}/>
            Поисковый узор снаружи
          </Menu.Item>
        </Menu>
      </div>
    );
  }

  getPicker() {
    const {selectedColorType} = this.state;
    const selectedColor = this.props.qrOptions.get(`color${selectedColorType}`);

    return (
      <div className='colors-tab__picker-popup' style={{left: this.state.pickerCoords[0], top: this.state.pickerCoords[1]}}>
        <SketchPicker onChange={this.onColorChange} color={selectedColor} colors={[]} />
      </div>
    );
  }

  render() {
    const {selectedColorType} = this.state;
    const selectedColor = this.props.qrOptions.get(`color${selectedColorType}`);

    return (
      <div className='colors-tab' >
        {this.colorTypeSelector()}
        <div className='colors-tab__color-palette'>
          {this.state.pickerVisible ? this.getPicker() : null}
          <CirclePicker onChange={this.onColorChange} color={selectedColor} colors={COLORS}/>
          <Button className='colors-tab__show-picker-btn' circular icon='options' onClick={this.showPicker} />
        </div>

        <div className='colors-tab__footer-buttons'>
          <Button  content='Мне повезёт!' icon='random' labelPosition='right' onClick={this.makeRandom} />
          <Button  content='Сбросить' icon='undo' labelPosition='right' onClick={this.reset} />
        </div>
      </div>
    );
  }
}

