import {} from 'styles/customization/CustomizationView.css';

import React, { Component } from 'react';
import { Step, Grid } from 'semantic-ui-react'
import QRCodeView from 'components/QRCodeView';
import {connect} from 'react-redux';
import Tabs, { TabPane } from 'rc-tabs';
import TabContent from 'rc-tabs/lib/TabContent';
import InkTabBar from 'rc-tabs/lib/InkTabBar';
import {bindActionCreators} from 'redux';
import * as AppActions from 'redux/actions';
import GeometryTab from './GeometryTab';
import ColorsTab from './ColorsTab';
import ImagesTab from './ImagesTab';

@connect(
  state =>  ({
    qrCode: state.app.get('qrCode'),
    qrContent: state.app.get('qrContent'),
    qrOptions: state.app.get('qrOptions'),
    qrImages: state.app.get('qrImages')
  }),
  dispatch => ({
    appActions: bindActionCreators(AppActions, dispatch)
  })
)
export default class CustomizationView extends Component {
  render() {
    return (
      <Grid className='customization-view' >
        <Grid.Row columns='2'>
          <Grid.Column className='customization-view__qrcode'>
            <QRCodeView {...this.props} />
          </Grid.Column>

          <Grid.Column className='customization-view__settings'>
            <Tabs defaultActiveKey='1' renderTabBar={()=><InkTabBar />} renderTabContent={()=><TabContent />}>
              <TabPane tab='Геометрия' key='1'><GeometryTab {...this.props}/></TabPane>
              <TabPane tab='Цвета' key='2'><ColorsTab {...this.props}/></TabPane>
              <TabPane tab='Картинки' key='3'><ImagesTab {...this.props}/></TabPane>
            </Tabs>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

