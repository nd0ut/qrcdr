import {} from 'styles/customization/GeometryTab.css';

import React, { Component } from 'react';
import { Step, Image, Dropdown } from 'semantic-ui-react'
import QRCodeView from 'components/QRCodeView';
import {connect} from 'react-redux';
import Tabs, { TabPane } from 'rc-tabs';
import TabContent from 'rc-tabs/lib/TabContent';
import InkTabBar from 'rc-tabs/lib/InkTabBar';
import Slider from 'rc-slider';
import {autobind, decorate} from 'core-decorators';
import { memoize, debounce, random } from 'lodash';
import { Form, Checkbox, Menu, Button } from 'semantic-ui-react'
import _ from 'lodash';

export default class GeometryTab extends Component {
  state = {
    roundInner: null,
    roundOuter: null,
    innerAngle: null,
    outerAngle: null
  }

  componentWillReceiveProps(nextProps) {
    this.state.roundInner !== nextProps.qrOptions.get('roundInner') && this.setState({roundInner: nextProps.qrOptions.get('roundInner')});
    this.state.roundOuter !== nextProps.qrOptions.get('roundOuter') && this.setState({roundOuter: nextProps.qrOptions.get('roundOuter')});
    this.state.innerAngle !== nextProps.qrOptions.get('innerAngle') && this.setState({innerAngle: nextProps.qrOptions.get('innerAngle')});
    this.state.outerAngle !== nextProps.qrOptions.get('outerAngle') && this.setState({outerAngle: nextProps.qrOptions.get('outerAngle')});
  }



  @decorate(memoize)
  onSliderChangeFn(propName) {
    return val => this.setState({[propName]: val}, _.throttle(() => this.applyGeometry(propName, val)));
  }

  @decorate(memoize)
  onSliderAfterChangeFn(propName) {
    return val => this.setState({[propName]: val}, () => this.applyGeometry(propName, val));
  }

  applyGeometry(propName, val) {
      this.props.appActions.setQrOptions({
        [propName]: val
      });
  }

  @autobind
  onShapeChange(e, {name}) {
    this.props.appActions.setQrOptions({
      shape: parseInt(name)
    });
  }

  getShapeSelector() {
    const shape = this.props.qrOptions.get('shape');

    return (
      <div className='geometry-tab__shape-select'>
        <Menu fluid vertical secondary>
          <Menu.Item name='0' active={shape === 0} onClick={this.onShapeChange}>
            <img className='geometry-tab__shape-icon' src='/images/shape_0.png' />
            Стандартное связывание
          </Menu.Item>

          <Menu.Item name='1' active={shape === 1} onClick={this.onShapeChange}>
            <img className='geometry-tab__shape-icon' src='/images/shape_1.png' />
            Полное связывание
          </Menu.Item>

          <Menu.Item name='2' active={shape === 2} onClick={this.onShapeChange}>
            <img className='geometry-tab__shape-icon' src='/images/shape_2.png' />
            Диагональное связывание
          </Menu.Item>

          <Menu.Item name='3' active={shape === 3} onClick={this.onShapeChange}>
            <img className='geometry-tab__shape-icon' src='/images/shape_3.png' />
            Без связывания
          </Menu.Item>

        </Menu>
      </div>
    );
  }

  getSliders() {
    return (
      <div className='geometry-tab__sliders'>
        <div className='geometry-tab__slider-group'>
          <div className='geometry-tab__slider-group-label'>Внешний радиус</div>
          <Slider
            className='geometry-tab__slider'
            min={-5} max={12} step={0.01}
            onAfterChange={this.onSliderAfterChangeFn('roundOuter')} onChange={this.onSliderChangeFn('roundOuter')}
            value={this.state.roundOuter}
          />
        </div>

        <div className='geometry-tab__slider-group'>
          <div className='geometry-tab__slider-group-label'>Внутренний радиус</div>
          <Slider
            className='geometry-tab__slider'
            min={-5} max={12} step={0.01}
            onAfterChange={this.onSliderAfterChangeFn('roundInner')} onChange={this.onSliderChangeFn('roundInner')}
            value={this.state.roundInner}
          />
        </div>

        <div className='geometry-tab__slider-group'>
          <div className='geometry-tab__slider-group-label'>Внешний наклон</div>
          <Slider
            className='geometry-tab__slider'
            min={-1} max={1} step={0.01}
            onAfterChange={this.onSliderAfterChangeFn('outerAngle')} onChange={this.onSliderChangeFn('outerAngle')}
            value={this.state.outerAngle}
          />
        </div>

        <div className='geometry-tab__slider-group'>
          <div className='geometry-tab__slider-group-label'>Внутренний наклон радиус</div>
          <Slider
            className='geometry-tab__slider'
            min={-1} max={1} step={0.01}
            onAfterChange={this.onSliderAfterChangeFn('innerAngle')} onChange={this.onSliderChangeFn('innerAngle')}
            value={this.state.innerAngle}
          />
        </div>
      </div>
    );
  }

  @autobind
  makeRandom() {
    const randomOptions = {
      shape: random(0, 3),
      roundOuter: random(-5, 12, true),
      roundInner: random(-5, 12, true),
      outerAngle: random(-1, 1, true),
      innerAngle: random(-1, 1, true)
    };

    this.props.appActions.setQrOptions(randomOptions);
  }

  @autobind
  reset() {
    const defaultOptions = {
      shape: 0,
      roundOuter: 5,
      roundInner: 5,
      outerAngle: 0,
      innerAngle: 0
    };

    this.props.appActions.setQrOptions(defaultOptions);
  }

  @autobind
  handleLevelChange(e, {value}) {
    const qrContent = this.props.qrContent.toJS();

    this.props.appActions.setQrContent({
      ...qrContent,
      version: this.props.qrCode.get('version'),
      level: value
    });
  }

  @autobind
  handleVersionChange(e, {value}) {
    const qrContent = this.props.qrContent.toJS();

    this.props.appActions.setQrContent({
      ...qrContent,
      level: this.props.qrCode.get('level'),
      version: value
    });
  }

  getLevelCorrectionSelector() {
    const levels = [
      {text: 'Автоматически', value: 'auto'},
      {text: 'H', value: 'H'},
      {text: 'Q', value: 'Q'},
      {text: 'M', value: 'M'},
      {text: 'L', value: 'L'}
    ];

    const versions = [{text: 'Автоматически', value: 'auto'}].concat(_.range(1, 40 + 1).map(i => ({text: i, value: i})));

    return (
      <div className='geometry-tab__error-correction'>
        <Form>
          <Form.Group widths='equal'>
            <Form.Field>
              <label>Уровень коррекции</label>
              <Dropdown value={this.props.qrCode.level || 'auto'} search floating labeled button className='icon' options={levels} onChange={this.handleLevelChange} />
            </Form.Field>
            <Form.Field>
              <label>Номер версии</label>
              <Dropdown value={this.props.qrCode.version || 'auto'} search floating labeled button className='icon' options={versions} onChange={this.handleVersionChange} />
            </Form.Field>
          </Form.Group>
        </Form>
      </div>
    );
  }

  render() {
    if(!this.props.qrCode) {
      return null;
    }

    return (
      <div className='geometry-tab' >
        {this.getLevelCorrectionSelector()}
        {this.getShapeSelector()}
        {this.getSliders()}

        <div className='geometry-tab__footer-buttons'>
          <Button content='Мне повезёт!' icon='random' labelPosition='right' onClick={this.makeRandom} />
          <Button content='Сбросить' icon='undo' labelPosition='right' onClick={this.reset} />
        </div>
      </div>
    );
  }
}

