import {} from 'styles/customization/ImagesTab.css';

import * as d3 from 'd3';
import React, { Component } from 'react';
import { Step, Image } from 'semantic-ui-react'
import QRCodeView from 'components/QRCodeView';
import {connect} from 'react-redux';
import Tabs, { TabPane } from 'rc-tabs';
import TabContent from 'rc-tabs/lib/TabContent';
import InkTabBar from 'rc-tabs/lib/InkTabBar';
import {autobind, decorate, throttle} from 'core-decorators';
import { memoize, debounce, random } from 'lodash';
import { Form, Checkbox, Menu, Button, Segment, List, Radio, Label } from 'semantic-ui-react'
import Dropzone from 'react-dropzone';
import QArt from 'react-qart';
import SwitchButton from '../controls/Switch';
import Slider from 'rc-slider';

export default class ImagesTab extends Component {
  @autobind
  onDrop(files) {
    Promise.all(files.map(file => this.getBase64(file).then(base64 => ({file, base64}))))
      .then(this.addImages);
  }

  @autobind
  addImages(images) {
    images = images.map(({file, base64}) => ({
      id: `image-${new Date().getTime()}`,
      name: file.name,
      type: 'background',
      coords: [0,0],
      width: 50,
      opacity: 100,
      base64
    }));

    this.props.appActions.addQrImages(images);
  }

  getBase64(file) {
     return new Promise((res, rej) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = () => res(reader.result);
        reader.onerror = err => rej(err);
     });
  }

  @autobind
  onTypeChange(e) {
    const input = e.currentTarget.querySelector('input');
    const imageId = input.attributes.getNamedItem('name').value;
    const type = !input.checked ? 'logo' : 'background';

    this.props.appActions.updateQrImage(imageId, {type});
  }

  @autobind
  onWidthChange(imageId, width) {
    const image = d3.select('#svg-container image#' + imageId);
    image.attr('width', width + '%');
  }

  @autobind
  onWidthAfterChange(imageId, width) {
    this.props.appActions.updateQrImage(imageId, {width});
  }

  @autobind
  onOpacityChange(imageId, opacity) {
    const image = d3.select('#svg-container image#' + imageId);
    image.style('opacity', opacity / 100);
  }

  @autobind
  onOpacityAfterChange(imageId, opacity) {
    this.props.appActions.updateQrImage(imageId, {opacity: opacity / 100});
  }

  @autobind
  onImageRemove(e) {
    const imageId = e.target.closest('.item').attributes.getNamedItem('data-id').value
    this.props.appActions.removeQrImage(imageId);
  }

  getImageList() {
    const images = this.props.qrImages.toJS();

    return (
      <List divided selection>
        {images.map(image => (
          <List.Item data-id={image.id} key={image.name} className='images-tab_image-item'>
            <Image avatar src={image.base64}/>
            <List.Content>
              <List.Header>{image.name}</List.Header>
              <List.Description>
                <div className='images-tab_type-selector'>
                  <span>Режим: </span>
                  <label className={image.type === 'background' ? 'images-tab_type-selected' : null} htmlFor={image.id}>фон</label>
                  <Radio checked={image.type === 'logo'} slider name={image.id} onChange={this.onTypeChange} />
                  <label className={image.type === 'logo' ? 'images-tab_type-selected' : null} htmlFor={image.id}>логотип</label>
                </div>
                <div className='images-tab_slider'>
                  <span>Прозрачность:</span>
                  <Slider min={1} max={100} onAfterChange={w => this.onOpacityAfterChange(image.id, w)} onChange={w => this.onOpacityChange(image.id, w)} defaultValue={image.opacity}/>
                </div>
                <div className='images-tab_slider'>
                  <span>Размер:</span>
                  <Slider min={1} max={200} onAfterChange={w => this.onWidthAfterChange(image.id, w)} onChange={w => this.onWidthChange(image.id, w)} defaultValue={image.width}/>
                </div>
              </List.Description>
            </List.Content>
            <div className='images-tab_image-item-remove'>
              <Button negative circular icon='remove' size='tiny' onClick={this.onImageRemove}/>
            </div>
          </List.Item>
        ))}
      </List>
    );
  }

  render() {
    return (
      <div className='images-tab' >
        <Dropzone className='images-tab_drop-zone' onDrop={this.onDrop}>
          <Segment clearing>
            Перетащите файл сюда
            <Button icon='upload' floated='right' positive content='Загрузить' labelPosition='right' />
          </Segment>
        </Dropzone>

        <div className='images-tab_image-list'>
          {this.props.qrImages ? this.getImageList() : null}
        </div>
      </div>
    );
  }
}

