import {} from 'styles/ContentView.css';
import {} from 'styles/transitions.css';

import React, { Component } from 'react';
import { Button, Grid, Segment, Menu, Icon, Form, Input, Header, Checkbox, Dropdown, List, Divider, Label} from 'semantic-ui-react'
import ReactCSSTransitionReplace from 'react-css-transition-replace';
import {autobind} from 'core-decorators';
import * as AppActions from 'redux/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Link } from 'react-router'
import serializeForm from 'form-serialize';
import _ from 'lodash';
import DateRangePickerWrapper from './controls/DateRangePickerWrapper';
import ReactSelectWrapper from './controls/ReactSelectWrapper';
import moment from 'moment';
import Loader from 'halogen/ClipLoader';

@connect(
  state => {
    const qrContent = state.app.get('qrContent');
    return {
      activeItem: qrContent ? qrContent.get('type') : null,
      formData: qrContent ? qrContent.get('data').toJS() : {},
    }
  },
  dispatch => ({
    appActions: bindActionCreators(AppActions, dispatch)
  })
)
export default class ContentView extends Component {
  state = {
    activeItem: this.props.activeItem || 'text',
    geodata: {
      lat: null,
      lon: null,
      addr: null
    },
    cal: {
      startDate: null,
      endDate: null,
      focusedInput: null
    }
  }

  componendWillMount() {
    moment.locale('ru');
  }

  componentDidUpdate() {
    if(this.state.activeItem === 'geo') {
      !this.mapInitialized && setTimeout(() => this.initMap(), 300);
    } else {
      this.mapInitialized = false;
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.activeItem && nextProps.activeItem !== this.state.activeItem) {
      this.setState({activeItem: nextProps.activeItem});
    }
  }

  @autobind
  initMap() {
    const map = new ymaps.Map('map', {
      center: [55.76, 37.64],
      zoom: 7
    });

    const searchControl = new ymaps.control.SearchControl({
      options: {
        provider: 'yandex#search'
      }
    });

    map.controls.add(searchControl);

    map.events.add('click', e => {
        const coords = e.get('coords');
        const marker = new ymaps.GeoObject({
          geometry: {
            type: 'Point',
            coordinates: coords
          }
        });
        map.geoObjects.removeAll();
        map.geoObjects.add(marker);

        ymaps.geocode(coords)
          .then(res => {
            const addr = res.geoObjects.get(0).properties.get('metaDataProperty').GeocoderMetaData.text;
            const [lat, lon] = coords;
            const geodata = {lat, lon, addr}
            this.setState({geodata});
          })
    });

    this.mapInitialized = true;
    this.forceUpdate();
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  getMenuItem(id, title, icon, awesome = false) {
    return (
      <Menu.Item name={id} active={this.state.activeItem === id} onClick={this.handleItemClick} >
        {awesome ? <i className={`content-view__menu-icon fa fa-${icon}`} aria-hidden='true'></i> : <Icon name={icon} />}
        <span>{title}</span>
      </Menu.Item>
    );
  }

  getContactForm() {
    return (
      <span>
        <Header dividing size='small' textAlign='left'>
          <Icon name='user'/>
          Общие сведения
        </Header>
        <Form.Group widths='equal'>
          <Form.Input defaultValue={this.props.formData.name} name='name' placeholder='Имя' />
          <Form.Input defaultValue={this.props.formData.last_name} name='last_name' placeholder='Фамилия' />
          <Form.Input defaultValue={this.props.formData.patronymic_name} name='patronymic_name' placeholder='Отчество' />
        </Form.Group>
        <Form.Group widths='equal'>
          <Form.Input defaultValue={this.props.formData.organization} name='organization' placeholder='Организация' />
          <Form.Input defaultValue={this.props.formData.position} name='position' placeholder='Должность' />
        </Form.Group>

        <Header dividing size='small' textAlign='left'>
          <Icon name='call'/>
          Контактная информация
        </Header>
        <Form.Group widths='equal'>
          <Form.Input defaultValue={this.props.formData.mobile_phone} name='mobile_phone' placeholder='Мобильный телефон' />
          <Form.Input defaultValue={this.props.formData.work_phone} name='work_phone' placeholder='Рабочий телефон' />
        </Form.Group>
        <Form.Group widths='equal'>
          <Form.Input defaultValue={this.props.formData.email} name='email' placeholder='Email' />
          <Form.Input defaultValue={this.props.formData.url} name='url' placeholder='Веб-сайт' />
        </Form.Group>

        <Header dividing size='small' textAlign='left'>
          <Icon name='map'/>
          Адрес
        </Header>
        <Form.Group widths='equal'>
          <Form.Input defaultValue={this.props.formData.country} name='country' placeholder='Страна' />
          <Form.Input defaultValue={this.props.formData.city} name='city' placeholder='Город' />
        </Form.Group>
        <Form.Group widths='equal'>
          <Form.Input defaultValue={this.props.formData.street} name='street' placeholder='Улица' />
          <Form.Input defaultValue={this.props.formData.building} name='building' placeholder='Дом' />
        </Form.Group>

      </span>
    );
  }

  getWifiForm() {
    const securityTypes = [
      {text: 'WPA/WPA2', value: 'WPA'},
      {text: 'WEP', value: 'WEP'},
      {text: 'Без шифрования', value: 'nopass'}
    ];

    return (
      <div>
        <Form.Input defaultValue={this.props.formData.ssid} label='Имя сети:' name='ssid' />
        <Form.Checkbox defaultValue={this.props.formData.hidden} label='Скрытая' name='hidden' />
        <Form.Select defaultValue={this.props.formData.security || 'WPA'} name='security' label='Тип шифрования:' options={securityTypes}/>
        <Form.Input defaultValue={this.props.formData.password} label='Пароль:' type='password' name='password'/>
        <Form.Group widths='equal'>
          <Form.Checkbox label='Показать пароль' name='show_password' />
        </Form.Group>
      </div>
    );
  }

  getGeoForm() {
    const loadingPopup = (
      <div className='content-view__geo-loader'>
        <Loader size='96px' color='rgba(44, 62, 80,1.0)'/>
        <span>Карта загружается</span>
      </div>
    );
    return (
      <div>
        <Label>
          <Icon name='marker' />
            {this.state.geodata.addr ? this.state.geodata.addr : 'Выберете точку'}
            {this.state.geodata.lat ? <Label.Detail>{this.state.geodata.lat.toFixed(3)}, {this.state.geodata.lon.toFixed(3)}</Label.Detail> : null}
        </Label>

        <Segment>
          {this.mapInitialized ? null : loadingPopup}
          <div id='map' style={{width: '100%', height: '300px'}}/>
        </Segment>

        <Form.Input type='hidden' name='lat' defaultValue={this.state.geodata.lat}/>
        <Form.Input type='hidden' name='lon' defaultValue={this.state.geodata.lon}/>
      </div>
    );
  }

  getCalendarForm() {
    return (
      <div>
        <Form.Input defaultValue={this.props.formData.summary} label='Событие:' name='summary' />
        <Form.Field label='Дата' control={DateRangePickerWrapper}
          className='content-view__field-with-bottom-margin'
          initialStartDate={this.props.formData.startDate || moment()}
          initialEndDate={this.props.formData.endDate || moment()}
          showDefaultInputIcon
          startDatePlaceholderText='Начало'
          endDatePlaceholderText='Конец'
          phrases={{closeDatePicker: 'Закрыть', clearDates: 'Очистить'}}
          showClearDates
        />
      </div>
    );
  }

  getItemForm(itemId) {
    switch(itemId) {
      case 'text':
        return <Form.Input required defaultValue={this.props.formData.plain_text} name='plain_text' label='Введите текст:' />;
      case 'link':
        return (
          <Form.Field>
            <label>Введите адрес сайта:</label>
            <Input defaultValue={this.props.formData.url} name='url' label='http://' placeholder='yandex.ru'/>
          </Form.Field>
        );
      case 'contact':
        return this.getContactForm();
      case 'wifi':
        return this.getWifiForm();
      case 'sms':
        return (
          <div>
            <Form.Input defaultValue={this.props.formData.phone} label='Номер телефона:' name='phone'/>
            <Form.Input defaultValue={this.props.formData.text} label='Текст сообщения:' name='text'/>
          <Form.Group/>
          </div>
        );
      case 'email':
        return (
          <div>
            <Form.Input defaultValue={this.props.formData.email} label='Email:' name='email'/>
            <Form.Input defaultValue={this.props.formData.subject} label='Тема:' name='subject'/>
            <Form.Input defaultValue={this.props.formData.body} label='Текст письма:' name='body'/>
          <Form.Group/>
          </div>
        );
      case 'phone':
        return <Form.Input defaultValue={this.props.formData.phone} required name='phone' label='Введите номер:' />;
      case 'geo':
        return this.getGeoForm();
      case 'calendar':
        return this.getCalendarForm();
      case 'googleplay':
        return <Form.Field googleplay defaultValue={this.props.formData.package_name} name='package_name' control={ReactSelectWrapper} label='Выберите приложение: '/>;
      case 'youtube':
        return <Form.Field defaultValue={this.props.formData.video_id} youtube name='video_id' control={ReactSelectWrapper} label='Выберите видео: '/>;
    }
  }

  @autobind
  getForm() {
    return (
      <div className='content-view__form'>
        <Form onSubmit={this.onSubmit}>
          {this.getItemForm(this.state.activeItem)}
          <Button primary type='submit'>Продолжить</Button>
        </Form>
      </div>
    );
  }

  @autobind
  onSubmit(e) {
    e.preventDefault();
    e.stopPropagation();

    this.props.router.push('/customize');
    const form = e.target;
    const data = serializeForm(form, {hash: true});
    this.props.appActions.setQrContent({type: this.state.activeItem, data});
  }

  render() {
    const ItemForm = this.getForm;

    return (
      <Grid className='content-view' >
        <Grid.Row>
          <Grid.Column width={4}>
            <Menu pointing secondary vertical>
              {this.getMenuItem('text', 'Просто текст', 'file text outline')}
              {this.getMenuItem('link', 'Ссылка', 'linkify')}
              {this.getMenuItem('contact', 'Визитка', 'address-card-o', true)}
              {this.getMenuItem('wifi', 'Wifi', 'wifi')}
              {this.getMenuItem('sms', 'Отправить SMS', 'commenting-o', true)}
              {this.getMenuItem('email', 'Отправить e-mail', 'envelope-o', true)}
              {this.getMenuItem('phone', 'Позвонить на номер', 'phone', true)}
              {this.getMenuItem('geo', 'Геолокация', 'map-marker', true)}
              {this.getMenuItem('calendar', 'Календарь', 'calendar-plus-o', true)}
              {this.getMenuItem('googleplay', 'Google Play', 'google', true)}
              {this.getMenuItem('youtube', 'Youtube', 'youtube', true)}
            </Menu>
          </Grid.Column>

          <Grid.Column stretched width={12}>
            <ReactCSSTransitionReplace transitionName='carousel-swap' transitionEnterTimeout={300} transitionLeaveTimeout={300}>
              <ItemForm key={this.state.activeItem}/>
            </ReactCSSTransitionReplace>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

