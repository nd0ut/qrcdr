import {} from 'styles/QRCodeView.css';

import * as d3 from 'd3';
import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import _ from 'lodash';
import QRCodeRenderer from 'qr-engine/QRCodeRenderer';
import {connect} from 'react-redux';
import Slider from 'rc-slider';
import {autobind} from 'core-decorators';
import d3SaveSvg from 'd3-save-svg';
import {saveSvgAsPng} from 'save-svg-as-png'
// import Loader from 'halogen/ClipLoader';
import {Loader} from 'semantic-ui-react'

window.d3 = d3;

export default class QRCodeView extends Component {
  state = {
    qrRendered: false
  }

  componentDidMount() {
    setTimeout(() => this.qrRender(), 1000);
  }

  componentWillReceiveProps(nextProps) {
    this.forceUpdate();
  }

  componentDidUpdate() {
    this.qrRender();
  }

  qrRender() {
    const qrcode = this.props.qrCode;
    const options = this.props.qrOptions.toJS();

    if(!qrcode) {
      return;
    }

    const svg = QRCodeRenderer.render(qrcode, options);
    this.renderSvg(svg);

    !this.state.qrRendered && this.setState({qrRendered: true});
  }

  renderSvg(svg) {
    const container = d3.select(ReactDOM.findDOMNode(this.refs['svg-container']));
    container.html(svg);

    svg = container.select('svg');
    svg.attr('width', null);
    svg.attr('height', null);

    this.renderImages(svg);
  }

  renderImages(svg) {
    if(!this.props.qrImages) {
      return;
    }

    const images = this.props.qrImages.toJS();

    images.forEach(img => {
      svg
        .select(img.type === 'background' ? '#qr-image_bg' : '#qr-images')
        .append('svg:image')
        .attr('id', img.id)
        .attr('x', img.coords[0])
        .attr('y', img.coords[1])
        .attr('width', `${img.width}%`)
        .attr('xlink:href', img.base64)
        .style('opacity', img.opacity);
    });

    this.captureMouseEvents(svg);
  }

  captureMouseEvents(svg) {
    const self = this;

    const images = svg.selectAll('image');
    const drag = d3.drag()
      .container(function() {return this.parentNode} )
      .on('drag', this.drag)
      .on('end', function() { self.dragEnd.call(this, self) });

    images.call(drag);
  }

  dragEnd(self) {
    const e = d3.event;
    const image = d3.select(this);
    const x = parseFloat(image.attr('x')) + e.dx;
    const y = parseFloat(image.attr('y')) + e.dy;

    self.props.appActions.updateQrImage(image.attr('id'), {coords: [x, y]});
  }

  drag() {
    const e = d3.event;
    const image = d3.select(this);
    const x = parseFloat(image.attr('x')) + e.dx;
    const y = parseFloat(image.attr('y')) + e.dy;

    image
      .attr('x', x)
      .attr('y', y);
  }

  export(type) {
    if(type === 'svg') {
      const svg = d3.select(ReactDOM.findDOMNode(this.refs['svg-container'])).select('svg');
      d3SaveSvg.save(svg.node(), {filename: 'qr-code-exported'});
    }
    if(type === 'png') {
      const opts = {
        height: 46,
        width: 46,
        scale: 20
      };

      saveSvgAsPng(ReactDOM.findDOMNode(this.refs['svg-container']).querySelector('svg'), 'qr-code-exported', opts)
    }
  }

  render() {
    return (
      <div className='qr-code-view'>
        {this.state.qrRendered ? null : <div className='qr-code-view__loader'><Loader active size='massive'/></div>}
        <div ref='svg-container' id='svg-container'>
        </div>
      </div>
    );
  }
}

