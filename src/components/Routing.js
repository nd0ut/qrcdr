import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory, IndexRedirect } from 'react-router';
import { Provider } from 'react-redux'

import App from './App';
import CustomizationView from './customization/CustomizationView';
import ContentView from './ContentView';
import ExportView from './ExportView';

export default props => (
  <Provider store={props.store}>
    <Router history={hashHistory}>
      <Route path='/' component={App}>
        <IndexRedirect to="content" />
        <Route path='content' component={ContentView}/>
        <Route path='customize' component={CustomizationView}/>
        <Route path='export' component={ExportView}/>
      </Route>
    </Router>
  </Provider>
);
