import {} from 'styles/App.css';

import React, { Component } from 'react';
import QRCodeView from './QRCodeView';
import { Grid, Segment } from 'semantic-ui-react'
import AppHeader from './AppHeader';
import Steps from './Steps';
import { Router, Route, Link, browserHistory, IndexRedirect } from 'react-router'
import { RouteTransition, presets } from 'react-router-transition';

export default class App extends Component {
  render() {
    return (
      <Grid className='app'>
        <AppHeader/>
        <Steps {...this.props}/>

        <Grid.Row className='app-content'>
          <Grid.Column>
            <RouteTransition
              runOnMount={false}
              pathname={this.props.location.pathname}
              {...presets.pop}
            >
              {this.props.children}
            </RouteTransition>
          </Grid.Column>
        </Grid.Row>

      </Grid>
    );
  }
}

