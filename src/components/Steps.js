import {} from 'styles/Steps.css';

import React, { Component } from 'react';
import { Icon, Step, Button, Grid, Container} from 'semantic-ui-react'

export default class Steps extends Component {
  render() {
    const contentRoute = this.props.location.pathname === '/content';
    const customizeRoute = this.props.location.pathname === '/customize';
    const exportRoute = this.props.location.pathname === '/export';

    return (
      <Grid.Row centered>
        <Grid.Column>
          <div className='steps'>
            <Step.Group size='small'>
              <Step active={contentRoute} onClick={() => this.props.router.push('/content')}>
                <Icon name='file text' />
                <Step.Content>
                  <Step.Title>Контент</Step.Title>
                  <Step.Description>Выберите тип контента для QR-кода</Step.Description>
                </Step.Content>
              </Step>

              <Step disabled={contentRoute} active={customizeRoute} onClick={() => this.props.router.push('/customize')}>
                <Icon name='setting' />
                <Step.Content>
                  <Step.Title>Настройки</Step.Title>
                  <Step.Description>Настройте параметры отображения</Step.Description>
                </Step.Content>
              </Step>

              <Step disabled={contentRoute} active={exportRoute} onClick={() => this.props.router.push('/export')}>
                <Icon name='qrcode' />
                <Step.Content>
                  <Step.Title>Экспорт</Step.Title>
                  <Step.Description>Сохраните QR-код для дальнейшего использования</Step.Description>
                </Step.Content>
              </Step>
            </Step.Group>
          </div>
        </Grid.Column>
      </Grid.Row>
    );
  }
}

