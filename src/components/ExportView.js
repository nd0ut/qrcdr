import {} from 'styles/ExportView.css';

import React, { Component } from 'react';
import { Step, Grid, Button, Header, Icon, Form, Radio } from 'semantic-ui-react'
import QRCodeView from 'components/QRCodeView';
import {connect} from 'react-redux';
import Tabs, { TabPane } from 'rc-tabs';
import TabContent from 'rc-tabs/lib/TabContent';
import InkTabBar from 'rc-tabs/lib/InkTabBar';
import {bindActionCreators} from 'redux';
import * as AppActions from 'redux/actions';
import {autobind} from 'core-decorators';

@connect(
  state =>  ({
    qrCode: state.app.get('qrCode'),
    qrOptions: state.app.get('qrOptions'),
    qrImages: state.app.get('qrImages')
  }),
  dispatch => ({
    appActions: bindActionCreators(AppActions, dispatch)
  })
)
export default class ExportView extends Component {
  state = {
    selectedType: 'png'
  }

  @autobind
  handleChange(e, {value}) {
    this.setState({selectedType: value});
  }

  @autobind
  export(e) {
    e.preventDefault();
    e.stopPropagation();

    this.refs.qrcodeview.export(this.state.selectedType)
  }

  render() {
    return (
      <Grid className='export-view' >
        <Grid.Row>
          <Grid.Column className='export-view__qrcode'>
            <QRCodeView ref='qrcodeview' {...this.props} />
          </Grid.Column>

          <Grid.Column className='export-view__settings'>
            <Header as='h2'>
              <Icon name='save' />
              <Header.Content>Сохранение в файл</Header.Content>
            </Header>

            <Form>
              <Form.Field>
                <label>Выберите тип файла:</label>
              </Form.Field>

              <Form.Field>
                <Radio
                  label='SVG'
                  name='export_type'
                  value='svg'
                  checked={this.state.selectedType === 'svg'}
                  onChange={this.handleChange}
                />
              </Form.Field>
              <Form.Field>
              <Radio
                label='PNG'
                name='export_type'
                value='png'
                checked={this.state.selectedType === 'png'}
                onChange={this.handleChange}
              />
              </Form.Field>

              <Form.Button icon='download' positive onClick={this.export} content='Скачать'/>
            </Form>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

