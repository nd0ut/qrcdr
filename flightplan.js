// flightplan.js
var plan = require('flightplan');
var format = require('util').format;

plan.target('production',
  {
    host: '78.24.222.93',
    username: 'root',
    agent: process.env.SSH_AUTH_SOCK
  },
  {
    dir: '/root/qrcdr',
    repo: 'git@bitbucket.org:nd0ut/qrcdr.git'
  }
);

plan.remote('init', function(t) {
  var host = plan.runtime.options;
  t.exec(format('mkdir -p %s', host.dir));

  t.with(format('cd %s', host.dir), function() {
    t.exec(format('git clone %s ./', host.repo));
    t.exec(format('git checkout', host.branch));
  });
});

plan.remote(function(t) {
  var host = plan.runtime.options;

  t.with(format('cd %s', host.dir), function() {
    t.exec('git pull');
    t.exec('yarn');
    t.exec('yarn build');
    t.exec('pm2 stop server.production');
    t.exec('pm2 start server.production.js');
  });

});
